/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import estadistica.Estadistica;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static java.awt.BorderLayout.CENTER;

/**
 *
 * @author ANGEL RIOS
 */
public class GraficasUI extends javax.swing.JFrame {

    private JFreeChart chart;

    /**
     * Creates new form GraficasUI
     *
     * @param tipo Tipo de grafica = "Pastel", "Barras" o "Lineas"
     */
    public GraficasUI(String tipo) {
        initComponents();
        setTitle("Grafica de " + tipo);
        Estadistica graficaInfo = new Estadistica();

        switch (tipo) {
            case "Pastel":
                System.out.println(Estadistica.clases.size());
                pastel(Estadistica.clases, Estadistica.firelativa, Estadistica.fi);
                break;
            case "Barras":
                barras(Estadistica.clases, Estadistica.fi);
                break;
            default:
                lineas(Estadistica.clases, Estadistica.fi);
                break;
        }
    }

    /**
     * Crea en un panel un gráfico de pastel usando la informacion ingresada a
     * traves de los parametros de la funcion
     *
     * @param clases Arreglo con las clases (Meses)
     * @param firelativa Arreglo con la frecuencia relativa de visitas en porcentajes
     * @param fi Arreglo con la frecuencia de visitas al archivo
     */
    public void pastel(ArrayList<String> clases, ArrayList<String> firelativa, ArrayList<Integer> fi) {
        JPanel panel = new JPanel();
        jPanel1.add(panel);
        // Fuente de Datos
        DefaultPieDataset data = new DefaultPieDataset();
        for (int i = 0; i < clases.size(); i++) {

            data.setValue(clases.get(i) + " (" + firelativa.get(i) + ")", fi.get(i));

        }

        // Creando el Grafico
        chart = ChartFactory.createPieChart("Visitas mensuales", data, true, true, false);

        // Crear el Panel del Grafico con ChartPanel
        ChartPanel chartPanel = new ChartPanel(chart);
        panel.add(chartPanel);
        this.pack();
    }

    /**
     * Crea en un panel un diagrama de barras usando la informacion ingresada a
     * traves de los parametros de la funcion
     *
     * @param clases Arreglo con las clases (meses)
     * @param fi Arreglo con la frecuencia de visitas al archivo
     */
    public void barras(ArrayList<String> clases, ArrayList<Integer> fi) {
        JPanel panel = new JPanel();
        jPanel1.add(panel, CENTER);

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i = 0; i < clases.size(); i++) {
            dataset.setValue(fi.get(i), clases.get(i), "Mes");

        }

        // Creando el Grafico
        chart = ChartFactory.createBarChart3D("Visitas mensuales", null, "Total visitas", dataset, PlotOrientation.VERTICAL, true, true, false);

        // Mostrar Grafico
        ChartPanel chartPanel = new ChartPanel(chart);
        panel.add(chartPanel);
        this.pack();
    }

    /**
     * Crea en un panel un gráfico de lineas usando la informacion ingresada a
     * traves de los parametros de la funcion
     *
     * @param clases Arreglo con las clases (meses)
     * @param fi Arreglo con la frecuencia de visitas al archivo
     */
    public void lineas(ArrayList<String> clases, ArrayList<Integer> fi) {
        JPanel panel = new JPanel();
        jPanel1.add(panel, CENTER);

        DefaultCategoryDataset line_chart_dataset = new DefaultCategoryDataset();
        for (int i = 0; i < clases.size(); i++) {
            line_chart_dataset.addValue(fi.get(i), "Visitas", clases.get(i));

        }

        // Creando el Grafico
        chart = ChartFactory.createLineChart3D("Visitas mensuales",
                "Mes", "Visitas", line_chart_dataset, PlotOrientation.VERTICAL,
                true, true, false);

        // Mostrar Grafico
        ChartPanel chartPanel = new ChartPanel(chart, 1000, 620, 300, 200, 1024, 1000, false, true, true, false, true, true);
        panel.add(chartPanel);
    }

    /**
     * Esta funcion guarda el grafico en la ruta especificada por el usuario en formato JPG
     */
    public void guardarImagen() {
        javax.swing.JFileChooser jF1 = new javax.swing.JFileChooser();
        jF1.setSelectedFile(new File("Grafica.jpg"));
        FileNameExtensionFilter filtroImagen = new FileNameExtensionFilter("JPG, PNG & GIF", "jpg", "png", "gif");
        jF1.setFileFilter(filtroImagen);
        String ruta;
        try {
            if (jF1.showSaveDialog(null) == jF1.APPROVE_OPTION) {
                ruta = jF1.getSelectedFile().getAbsolutePath();
                ChartUtilities.saveChartAsJPEG(new File(ruta), chart, 1000, 620);
            }
        } catch (HeadlessException | IOException ex) {
            JOptionPane.showMessageDialog(this, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Grafica -");
        setPreferredSize(new java.awt.Dimension(700, 600));

        jPanel1.setLayout(new java.awt.BorderLayout());
        jScrollPane1.setViewportView(jPanel1);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jMenu1.setText("Archivo");

        jMenuItem1.setText("Guardar como");
        jMenuItem1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenuItem1MousePressed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem1MousePressed
        guardarImagen();
    }//GEN-LAST:event_jMenuItem1MousePressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
