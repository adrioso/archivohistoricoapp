/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import accesdatabase.AccesConnector;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ANGEL RIOS
 */
public class InicioUI extends javax.swing.JFrame {

    private String query;

    /**
     * Creates new form Inicio
     */
    public InicioUI() {
        initComponents();
        buttonGroup1.add(rdbtnArchivoJudicial);
        rdbtnArchivoJudicial.setSelected(true);
        buttonGroup1.add(rdbtnHospitalMental);
        buttonGroup1.add(rdbtnLibro);
        buttonGroup1.add(rdbtnCG);
        buttonGroup1.add(rdbtnGine);
        buttonGroup1.add(rdbtnjj);

        jPanel3.setLayout(new GridLayout(3, 2, 5, 5));

    }

    /**
     * Esta funcion retorna el tipo de expediente prestado o la interacción del
     * usuario, el tipo de dato retornado es una cadena 'string'(J, H, CG, G,
     * JJ, L) de la siguiente forma: <br>
     * <br>
     * J = Archivo judicial <br>
     * H = Archivo hospital mental <br>
     * CG = Consulta general <br>
     * G = Archivo ginecologia <br>
     * JJ = Archivo Jaime Jaramillos <br>
     * L = Libro <br>
     * <br>
     *
     * @return tipo
     */
    public String getTipo() {
        String tipo;
        if (rdbtnArchivoJudicial.isSelected()) {
            tipo = "J";
        } else if (rdbtnHospitalMental.isSelected()) {
            tipo = "H";
        } else if (rdbtnCG.isSelected()) {
            tipo = "CG";
        } else if (rdbtnGine.isSelected()) {
            tipo = "G";
        } else if (rdbtnjj.isSelected()) {
            tipo = "JJ";
        } else {
            tipo = "L";
        }
        return tipo;
    }

    /**
     * Esta función guarda en la base de datos el archivo o la interaccion del
     * usuario según su tipo, es decir, si es de tipo J (Archivo judicial) el
     * codigo de la interracion se guarda en la tabla de archivo judicial, el
     * único tipo que no necesita código es la consulta general
     *
     * @param codigo Codigo del expediente, archivo o libro
     * @param tipo Tipo del archivo o interaccion del usuario
     */
    public void registrarDoc(String codigo, String tipo) {
        if (tipo.equals("J")) {
            try {
                query = "INSERT into Archivos_judiciales(Código)values" + "('" + codigo + "')";
                AccesConnector.getInstance().getStatement().executeUpdate(query);
                JOptionPane.showMessageDialog(null, "Se ha ingresado un nuevo archivo a la base de datos");
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(InicioUI.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        if (tipo.equals("H")) {
            try {
                query = "INSERT into Archivos_hospital(Código)values" + "('" + codigo + "')";
                AccesConnector.getInstance().getStatement().executeUpdate(query);
                JOptionPane.showMessageDialog(null, "Se ha ingresado un nuevo archivo a la base de datos");
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(InicioUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (tipo.equals("CG")) {
            try {
                query = ("INSERT into Ginecologia(Código)values" + "('" + codigo + "')");
                AccesConnector.getInstance().getStatement().executeUpdate(query);
                JOptionPane.showMessageDialog(null, "Se ha ingresado un nuevo archivo a la base de datos");
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(InicioUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (tipo.equals("JJ")) {

            try {
                query = "INSERT into JaimeJaramillo(Código)values" + "('" + codigo + "')";
                AccesConnector.getInstance().getStatement().executeUpdate(query);
                JOptionPane.showMessageDialog(null, "Se ha ingresado un nuevo archivo a la base de datos");
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(InicioUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (tipo.equals("L")) {
            try {
                JOptionPane.showMessageDialog(null, "Debe ingresar datos del libro");
                String titulo = JOptionPane.showInputDialog("Ingese el titulo del libro");
                String aut = JOptionPane.showInputDialog("Ingrese el autor");
                String edi = JOptionPane.showInputDialog("Ingrese la editorial del libro");

                query = "insert into Libros(Código, Titulo, Autor, Editorial)values" + "('" + codigo + "','" + titulo + "','" + aut + "','" + edi + "')";
                AccesConnector.getInstance().getStatement().executeUpdate(query);
                JOptionPane.showMessageDialog(null, "Se ha ingresado un nuevo archivo a la base de datos");
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(InicioUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Esta función recibe como parametro la identificación del usuario para asi
     * buscarlo en la base de datos y retorna 'true' si se encuentra o 'false'
     * si no es encontrado
     *
     * @param id Identificacion del usuario
     * @return Boolean
     */
    public boolean buscarUsu(Long id) {
        boolean ret = false;
        try {
            ResultSet rs = AccesConnector.getInstance().getStatement().executeQuery("SELECT * FROM Usuarios");
            while (rs.next()) {
                String a = rs.getString("Id");
                if (String.valueOf(id).equals(a)) {
                    ret = true;
                    break;
                }
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(InicioUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
    }

    /**
     * Limpia todos los campos de texto del frame InicioUI
     */
    public void limpiar() {
        tfId.setText("");
        tfProposito.setText("");
        tfCodigo.setText("");
        tfBusquedaArchivo.setText("");
        tfBusquedaUsuario.setText("");

    }

    /**
     * Recibe el tipo del expediente o interaccion del usuario y devuelve el
     * texto completo, es decir, si recibe 'J' retornara "Archivo judicial", de
     * la siguiente manera: <br>
     * <br>
     * L = Libro <br>
     * H = Archivo hospital mental <br>
     * J = Archivo judicial <br>
     * G = Ginecologia <br>
     * JJ = Jaime Jaramillo <br>
     * CG = Consulta general <br>
     * <br>
     *
     * @param tipo Tipo del archivo o interaccion del usuario
     * @return String
     */
    public static String tra(String tipo) {
        String bd;
        switch (tipo) {
            case "L":
                bd = "Libro";
                break;
            case "H":
                bd = "Archivo hospital mental";
                break;
            case "J":
                bd = "Archivo judicial";
                break;
            case "G":
                bd = "Ginecologia";
                break;
            case "JJ":
                bd = "Jaime Jaramillo";
                break;
            default:
                bd = "Consulta general";
                break;
        }
        return bd;

    }

    /**
     * Esta función busca en la base de datos si un expediente ya se encuentra
     * registrado, retornando 'true' si esta registrado o 'false' si no se
     * encuentra registrado
     *
     * @param codigo Codigo del expediente, archivo o libro
     * @param tipo Tipo del archivo o interaccion del usuario
     * @return boolean
     */
    public boolean buscarDoc(String codigo, String tipo) {
        boolean ret = false;
        String bd = "";
        switch (tipo) {
            case "L":
                bd += "Libros";
                break;
            case "H":
                bd += "Archivos_hospital";
                break;
            case "J":
                bd += "Archivos_judiciales";
                break;
            case "G":
                bd += "Ginecologia";
                break;
            case "JJ":
                bd += "JaimeJaramillo";
                break;
            case "CG":
                return true;
            default:
                break;
        }
        try {
            ResultSet rs = AccesConnector.getInstance().getStatement().executeQuery("SELECT * FROM " + bd);
            while (rs.next()) {
                String a = rs.getString("Código");
                if (String.valueOf(codigo).equals(a)) {
                    ret = true;
                    break;
                }
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(InicioUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
    }

    /**
     * Esta función registra la interaccion de un usuario, siendo esta un
     * prestamo de un expediente, un libro o una consulta general. <br> Si el
     * usuario es nuevo se registra en la base de datos, si el expediente es
     * nuevo, se registra automaticamente en la base de datos, si ambos tanto
     * expediente como usuario ya estan registrados, solo se registra la
     * interacción
     * <br>
     * <br> NOTA: 'registrar la interaccion' se refiere a guardar la fecha de
     * prestamo, el codigo del expediente y la identificacion del usuario en la
     * base de datos
     *
     * @param codigoExpediente Codigo del expediente, archivo o libro
     * @param tipo Tipo del archivo o interaccion del usuario
     * @param proposito Proposito o motivacion del prestamo
     * @param idUsuario Identificacion de usuario
     */
    public void registrar(String codigoExpediente, String tipo, String proposito, String idUsuario) {
        if (!("".equals(codigoExpediente)) || tipo.equals("CG")) {
            java.util.Date date = new java.util.Date();
            SimpleDateFormat f = new SimpleDateFormat("MM/dd/yyyy");
            String fecha = f.format(date);

            try {
                Long ide = Long.parseLong(idUsuario);
                /**
                 * Condicional cuando el usuario y el expediente se encuentran
                 * registrados en la base de datos
                 */
                if (buscarDoc(codigoExpediente, tipo) && buscarUsu(ide)) {
                    try {
                        //Se registra en la base de datos que el expediente fue prestado por cierto usuario
                        query = "insert into fechas_Archivos(Codigo, Fecha, Usuario, Tipo, Proposito)values" + "('" + codigoExpediente + "', #" + fecha + "#,'" + idUsuario + "','" + tipo + "','" + proposito + "')";
                        AccesConnector.getInstance().getStatement().executeUpdate(query);
                        JOptionPane.showMessageDialog(null, "Datos ingresados correctamente");
                    } catch (ClassNotFoundException | SQLException ex) {
                        JOptionPane.showMessageDialog(null, "Se ha presentado un error " + ex);
                    }
                    /**
                     * Condicional cuando el usuario se encuentra en la base de
                     * datos y el expediente no
                     */
                } else if (!(buscarDoc(codigoExpediente, tipo)) && buscarUsu(ide)) {
                    registrarDoc(codigoExpediente, tipo);
                    try {
                        //Se registra en la base de datos que el expediente fue prestado por cierto usuario
                        query = "insert into fechas_Archivos(Codigo, Fecha, Usuario, Tipo, Proposito)values" + "('" + codigoExpediente + "',#" + fecha + "#,'" + idUsuario + "','" + tipo + "','" + proposito + "')";
                        AccesConnector.getInstance().getStatement().executeUpdate(query);
                        JOptionPane.showMessageDialog(null, "Datos ingresados correctamente");
                    } catch (ClassNotFoundException | SQLException ex) {
                        JOptionPane.showMessageDialog(null, "Se ha presentado un error " + ex);
                    }
                    /**
                     * Condicional cuando el usuario no esta registrado en la
                     * base de datos y el expediente si esta registrado
                     */
                } else if (buscarDoc(codigoExpediente, tipo) && !(buscarUsu(ide))) {

                    /**
                     * Se crea el frame donde se ingresan los datos del nuevo
                     * usuario y se muestra en pantalla
                     */
                    CrearUsuarioUI frame = new CrearUsuarioUI(idUsuario);
                    frame.setVisible(true);

                    try {
                        //Se registra en la base de datos que el expediente fue prestado por cierto usuario
                        query = "insert into fechas_Archivos(Codigo, Fecha, Usuario, Tipo, Proposito)values" + "('" + codigoExpediente + "',#" + fecha + "#,'" + idUsuario + "','" + tipo + "','" + proposito + "')";
                        AccesConnector.getInstance().getStatement().executeUpdate(query);
                        JOptionPane.showMessageDialog(null, "Datos ingresados correctamente");

                    } catch (ClassNotFoundException | SQLException ex) {
                        JOptionPane.showMessageDialog(null, "Se ha presentado un error " + ex);
                    }
                    /**
                     * Condicional cuando el usuario y el expediente no se
                     * encuentran registrados en la base de datos
                     */
                } else if (!(buscarDoc(codigoExpediente, tipo)) && !(buscarUsu(ide))) {
                    try {
                        /**
                         * Se crea el frame donde se ingresan los datos del
                         * nuevo usuario y se muestra en pantalla
                         */
                        CrearUsuarioUI frame = new CrearUsuarioUI(idUsuario);
                        frame.setVisible(true);
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Error: " + e);
                    }
                    //Se registra el expediente en la base de datos
                    registrarDoc(tfCodigo.getText(), getTipo());
                    try {
                        //Se registra en la base de datos que el expediente fue prestado por cierto usuario
                        query = "insert into fechas_Archivos(Codigo, Fecha, Usuario, Tipo, Proposito)values" + "('" + codigoExpediente + "',#" + fecha + "#,'" + idUsuario + "','" + tipo + "','" + proposito + "')";
                        AccesConnector.getInstance().getStatement().executeUpdate(query);
                        JOptionPane.showMessageDialog(null, "Datos ingresados correctamente");
                    } catch (ClassNotFoundException | SQLException ex) {
                        JOptionPane.showMessageDialog(null, "Se ha presentado un error " + ex);
                    }
                }

            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Ingrese una identificación valida ");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Debe ingresar un código de expediente");
        }
    }

    /**
     * busca en la base de datos un usuario con identificacion 'idUsuario' si lo
     * encuentra muestra un 'frame' con toda la información de este
     *
     * @param idUsuario Identificacion del usuario
     */
    public void mostrarUsuario(String idUsuario) {
        long id;
        try {
            id = Long.parseLong(idUsuario);
            if (buscarUsu(id)) {
                new MUsuarioUI(id + "").setVisible(true);
                limpiar();
            } else {
                JOptionPane.showMessageDialog(this, "No se ha encontrado ningun usuario con la informacíón ingresada");
            }

        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(null, "Debe ingresar una identificación valida " + ex);
        }
    }

    /**
     * Esta funcion busca en la base de datos un expediente según su codigo y
     * tipo pera luego mostrar toda su informacion en un 'frame'
     *
     * @param codigoExpediente Codigo del expediente, archivo o libro
     */
    public void mostrarExpediente(String codigoExpediente) {
        String[] bases = {"Archivo judicial", "Archivo hospital mental", "Libro", "Ginecologia", "Jaime Jaramillo"};
        Object respuesta = JOptionPane.showInputDialog(null, "Tipo del archivo a buscar", "Seleccione", JOptionPane.DEFAULT_OPTION, null, bases, bases[0]);
        if (respuesta != null) {
            String tipo = respuesta.toString();
            if ("Archivo judicial".equals(tipo)) {
                tipo = "J";
            } else if ("Archivo hospital mental".equals(tipo)) {
                tipo = "H";
            } else if ("Libro".equals(tipo)) {
                tipo = "L";
            } else if ("Ginecologia".equals(tipo)) {
                tipo = "G";
            } else if ("Jaime Jaramillo".equals(tipo)) {
                tipo = "JJ";
            }

            if (buscarDoc(codigoExpediente, tipo)) {
                new MArchivoUI(codigoExpediente, tipo).setVisible(true);
                limpiar();
            } else {
                JOptionPane.showMessageDialog(this, "No se ha encontrado ningun expediente con la informacíon ingresada");
            }

        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        tfBusquedaUsuario = new javax.swing.JTextField();
        Expediente = new javax.swing.JLabel();
        tfBusquedaArchivo = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        rdbtnArchivoJudicial = new javax.swing.JRadioButton();
        rdbtnLibro = new javax.swing.JRadioButton();
        rdbtnGine = new javax.swing.JRadioButton();
        rdbtnHospitalMental = new javax.swing.JRadioButton();
        rdbtnCG = new javax.swing.JRadioButton();
        rdbtnjj = new javax.swing.JRadioButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        tfCodigo = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        tfId = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        tfProposito = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        btnRegistrar = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu6 = new javax.swing.JMenu();
        jMenuItem8 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        jMenuItem9 = new javax.swing.JMenuItem();

        jMenuItem4.setText("jMenuItem4");

        jMenu5.setText("jMenu5");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Base de datos - Archivo historico");
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Busqueda"));
        java.awt.GridBagLayout jPanel1Layout = new java.awt.GridBagLayout();
        jPanel1Layout.columnWidths = new int[] {0, 23, 0, 23, 0, 23, 0, 23, 0, 23, 0, 23, 0, 23, 0, 23, 0, 23, 0, 23, 0, 23, 0, 23, 0, 23, 0};
        jPanel1Layout.rowHeights = new int[] {0, 9, 0, 9, 0, 9, 0, 9, 0};
        jPanel1.setLayout(jPanel1Layout);

        jLabel1.setText("ID Usuario");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel1.add(jLabel1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 21;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(tfBusquedaUsuario, gridBagConstraints);

        Expediente.setText("Expediente");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel1.add(Expediente, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 21;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(tfBusquedaArchivo, gridBagConstraints);

        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 23;
        jPanel1.add(btnBuscar, gridBagConstraints);

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_END);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Registro"));
        jPanel2.setLayout(new java.awt.BorderLayout());

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Tipo"));
        java.awt.GridBagLayout jPanel3Layout = new java.awt.GridBagLayout();
        jPanel3Layout.columnWidths = new int[] {0, 23, 0, 23, 0, 23, 0};
        jPanel3Layout.rowHeights = new int[] {0, 9, 0, 9, 0};
        jPanel3.setLayout(jPanel3Layout);

        rdbtnArchivoJudicial.setText("Archivo judicial");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel3.add(rdbtnArchivoJudicial, gridBagConstraints);

        rdbtnLibro.setText("Libro");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel3.add(rdbtnLibro, gridBagConstraints);

        rdbtnGine.setText("Ginecología");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel3.add(rdbtnGine, gridBagConstraints);

        rdbtnHospitalMental.setText("Hospital mental");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel3.add(rdbtnHospitalMental, gridBagConstraints);

        rdbtnCG.setText("Consulta general");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        jPanel3.add(rdbtnCG, gridBagConstraints);

        rdbtnjj.setText("Jaime Jaramillo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        jPanel3.add(rdbtnjj, gridBagConstraints);

        jPanel2.add(jPanel3, java.awt.BorderLayout.LINE_START);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos"));
        java.awt.GridBagLayout jPanel4Layout = new java.awt.GridBagLayout();
        jPanel4Layout.columnWidths = new int[] {0, 23, 0, 23, 0, 23, 0, 23, 0, 23, 0, 23, 0, 23, 0, 23, 0, 23, 0, 23, 0};
        jPanel4Layout.rowHeights = new int[] {0, 9, 0, 9, 0};
        jPanel4.setLayout(jPanel4Layout);

        jLabel3.setText("Expediente");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel4.add(jLabel3, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 15;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel4.add(tfCodigo, gridBagConstraints);

        jLabel4.setText("ID Usuario");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel4.add(jLabel4, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 15;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel4.add(tfId, gridBagConstraints);

        jLabel5.setText("Proposito");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel4.add(jLabel5, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 15;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel4.add(tfProposito, gridBagConstraints);

        jPanel2.add(jPanel4, java.awt.BorderLayout.CENTER);

        btnRegistrar.setText("Registrar");
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });
        jPanel5.add(btnRegistrar);

        jPanel2.add(jPanel5, java.awt.BorderLayout.PAGE_END);

        getContentPane().add(jPanel2, java.awt.BorderLayout.CENTER);

        jMenu6.setText("Archivo");

        jMenuItem8.setText("Abrir base de datos");
        jMenuItem8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenuItem8MousePressed(evt);
            }
        });
        jMenu6.add(jMenuItem8);
        jMenu6.add(jSeparator1);

        jMenuItem7.setText("Salir");
        jMenuItem7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenuItem7MousePressed(evt);
            }
        });
        jMenu6.add(jMenuItem7);

        jMenuBar1.add(jMenu6);

        jMenu2.setText("Ver");

        jMenuItem1.setText("Expedientes");
        jMenuItem1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenuItem1MousePressed(evt);
            }
        });
        jMenu2.add(jMenuItem1);

        jMenuItem2.setText("Usuarios");
        jMenuItem2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenuItem2MousePressed(evt);
            }
        });
        jMenu2.add(jMenuItem2);
        jMenu2.add(jSeparator2);

        jMenu3.setText("Estadisticas");

        jMenuItem3.setText("Tabla de frecuencias");
        jMenuItem3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenuItem3MousePressed(evt);
            }
        });
        jMenu3.add(jMenuItem3);

        jMenuItem10.setText("Graficas");
        jMenuItem10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenuItem10MousePressed(evt);
            }
        });
        jMenu3.add(jMenuItem10);

        jMenu2.add(jMenu3);

        jMenuBar1.add(jMenu2);

        jMenu4.setText("Acerca de...");

        jMenuItem5.setText("Desarrollo y soporte");
        jMenuItem5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenuItem5MousePressed(evt);
            }
        });
        jMenu4.add(jMenuItem5);

        jMenuItem6.setText("Documentación");
        jMenuItem6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenuItem6MousePressed(evt);
            }
        });
        jMenu4.add(jMenuItem6);
        jMenu4.add(jSeparator3);

        jMenuItem9.setText("Manual de usuario");
        jMenuItem9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenuItem9MousePressed(evt);
            }
        });
        jMenu4.add(jMenuItem9);

        jMenuBar1.add(jMenu4);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed
        registrar(tfCodigo.getText(), getTipo(), tfProposito.getText(), tfId.getText());
        limpiar();
    }//GEN-LAST:event_btnRegistrarActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed

        if (!(tfBusquedaUsuario.getText().equals(""))) {
            mostrarUsuario(tfBusquedaUsuario.getText());
        } else if (!(tfBusquedaArchivo.getText().equals(""))) {
            this.mostrarExpediente(tfBusquedaArchivo.getText());
        } else {
            JOptionPane.showMessageDialog(null, "Llene alguno de los dos campos de busqueda");
        }


    }//GEN-LAST:event_btnBuscarActionPerformed

    private void jMenuItem7MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem7MousePressed
        System.exit(0);
    }//GEN-LAST:event_jMenuItem7MousePressed

    private void jMenuItem2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem2MousePressed
        new MostrarUsuariosUI().setVisible(true);
    }//GEN-LAST:event_jMenuItem2MousePressed

    private void jMenuItem1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem1MousePressed
        new MostrarExpedientes().setVisible(true);
    }//GEN-LAST:event_jMenuItem1MousePressed

    private void jMenuItem3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem3MousePressed
        new TablaFrecuencias().setVisible(true);
    }//GEN-LAST:event_jMenuItem3MousePressed

    private void jMenuItem5MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem5MousePressed
        new AcercaDe().setVisible(true);
    }//GEN-LAST:event_jMenuItem5MousePressed

    private void jMenuItem6MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem6MousePressed
        try {
            File path = new File("javadoc/index.html");
            Desktop.getDesktop().open(path);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "Se ha presentado un error: " + ex);
        }
    }//GEN-LAST:event_jMenuItem6MousePressed

    private void jMenuItem8MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem8MousePressed
        try {
            File path = new File("bdArchivo.accdb");
            Desktop.getDesktop().open(path);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "Se ha presentado un error: " + ex);
        }
    }//GEN-LAST:event_jMenuItem8MousePressed

    private void jMenuItem9MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem9MousePressed
        try {
            File path = new File("GuiaUsuario.pdf");
            Desktop.getDesktop().open(path);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "Se ha presentado un error: " + ex);
        }
    }//GEN-LAST:event_jMenuItem9MousePressed

    private void jMenuItem10MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem10MousePressed
        String[] graficas = {"Pastel", "Barras", "Lineas"};
        Object respuesta = JOptionPane.showInputDialog(this, "Tipo de gráfico", "Seleccione", JOptionPane.DEFAULT_OPTION, null, graficas, graficas[2]);
        if (respuesta != null) {
            new GraficasUI(respuesta.toString()).setVisible(true);
        }

    }//GEN-LAST:event_jMenuItem10MousePressed

    /**
     * Funcion principal del proyecto
     *
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InicioUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InicioUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InicioUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InicioUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new AccesConnector();
                    new InicioUI().setVisible(true);
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(InicioUI.class.getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(null, "Se ha producido un eror: " + ex);
                    System.exit(0);
                }

            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Expediente;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnRegistrar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JRadioButton rdbtnArchivoJudicial;
    private javax.swing.JRadioButton rdbtnCG;
    private javax.swing.JRadioButton rdbtnGine;
    private javax.swing.JRadioButton rdbtnHospitalMental;
    private javax.swing.JRadioButton rdbtnLibro;
    private javax.swing.JRadioButton rdbtnjj;
    private javax.swing.JTextField tfBusquedaArchivo;
    private javax.swing.JTextField tfBusquedaUsuario;
    private javax.swing.JTextField tfCodigo;
    private javax.swing.JTextField tfId;
    private javax.swing.JTextField tfProposito;
    // End of variables declaration//GEN-END:variables
}
