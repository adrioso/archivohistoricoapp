/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesdatabase;
/**
 *
 * @author ANGEL RIOS
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * This class is a connection channel between the program and the database. It
 * uses the singleton pattern in order to ensure the existence of one instance
 * of the class
 *
 */
public class AccesConnector {

    // Server port and database information 
    private final String serverPath;
    private final String dataBaseName;

    // Connection instances with JDBC
    private final String driver;
    private final String dataBase;
    private final Connection connection;
    private final Statement statement;

    // Singleton instance
    private static AccesConnector instance;

    /**
     * This method constructs the instance of the connector with the specified
     * requirements and information of the server.
     *
     * @throws SQLException The method returns the this exception when a
     * database error occurs.
     * @throws ClassNotFoundException The method returns the this exception when
     * a the class is not found in the database connector reference.
     */
    public AccesConnector() throws ClassNotFoundException, SQLException {
        serverPath = System.getProperty("user.dir");
        dataBaseName = "bdArchivo.accdb";

        
        /**
         *En el proyecto debo cargar la libreria .jar
         *mysql-connector-java-5.1.42-bin.jar
         *para que se cargue con el comando Class.forName()
         */
        driver = "net.ucanaccess.jdbc.UcanaccessDriver";
        dataBase = "jdbc:ucanaccess://" + serverPath + "/" + dataBaseName;
        

        Class.forName(driver);
        connection = DriverManager.getConnection(dataBase);
        statement = connection.createStatement();
    }

    /**
     * Singleton method of the class in order to ensure a unique instance.
     *
     * @return The method returns the instance of the class. It is important to
     * notice that this instance is unique in the server.
     * @throws SQLException The method returns the this exception when a
     * database error occurs.
     * @throws ClassNotFoundException The method returns the this exception when
     * a the class is not found in the database connector reference.
     */
    public static AccesConnector getInstance() throws ClassNotFoundException, SQLException {
        if (instance == null) {
            instance = new AccesConnector();
            return instance;
        } else {
            return instance;
        }
    }

    /**
     * Getter method to access to statement object. This object is necessary to
     * execute some queries.
     *
     * @return The method returns a reference to the statement object that will
     * be used to execute some queries.
     */
    public Statement getStatement() {
        return statement;
    }

    /**
     * This method is intended to retrieve the connection object in order to
     * make PreparedStatement references and make more easy the insertion of
     * queries in the controller classes.
     *
     * @return The method returns the connection object with the database after
     * that the constructor enables the connection.
     */
    public Connection getConnection() {
        return connection;
    }
}
