/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estadistica;

import accesdatabase.AccesConnector;

import javax.swing.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author ANGEL RIOS
 */
public class Estadistica {

    public static ArrayList<String> clases = new ArrayList<>();
    public static ArrayList<Integer> fi = new ArrayList<>();
    public static ArrayList<String> firelativa = new ArrayList<>();
    public static ArrayList<Integer> facum = new ArrayList<>();
    public static ArrayList<String> facumrelativa = new ArrayList<>();
    public static ArrayList<String> filtros = new ArrayList<>();

    private static String filtro;
    /**
     * El constructor de la clase, limpia el contenido de cada uno de los arreglos
     * y lee los datos para cargarlos nuevamente con la información mas reciente
     */
    public Estadistica(){
        clases.clear();
        fi.clear();
        firelativa.clear();
        facum.clear();
        facumrelativa.clear();
        filtros.clear();
        filtro = null;
        leerdatos();
    }

    /**
     * Lee la informacion de la base de datos para asi crear el fitro y las
     * clases
     *
     * @return ArrayList filtros
     */
    private ArrayList<String> leerdatos() {
        ArrayList<String> temp = new ArrayList<>();
        try {

            ResultSet rs = AccesConnector.getInstance().getStatement().executeQuery("SELECT * FROM fechas_Archivos ORDER BY Fecha DESC");
            while (rs.next()) {
                String fecha = rs.getString("Fecha");

                if (fecha != null) {
                    temp.add(fecha.substring(0, 10));
                    if (!(filtros.contains(año(fecha.substring(0, 10))))) {
                        filtros.add(año(fecha.substring(0, 10)));
                    }
                }
            }
        } catch (ClassNotFoundException | SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        crearFiltro();
        crearFrecuencias(temp);
        return filtros;
    }

    /**
     * La funcion filtro devuelve un booleano dependiendo de si la fecha
     * ingresada cumple con el filtro creado para los diagramas
     *
     * @param año Año de interaccion
     * @return True si es igual al filtro, false si es diferente al filtro
     */
    public static boolean filtro(String año) {
        if (filtro == null) {
            return true;
        } else if (año.equals(filtro)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Crea el filtro de las fechas que se mostraran en las graficas o tablas le
     * pide al usuario que ingrese una respuesta a esta cuestion por medio de un
     * JOptionPane
     */
    private void crearFiltro() {
        String[] graficas = new String[filtros.size() + 1];
        graficas[0] = "Sin filtro";

        for (int i = 0; i < filtros.size(); i++) {
            graficas[i + 1] = filtros.get(i);
        }

        Object respuesta = JOptionPane.showInputDialog(null, "Filtro por año", "Seleccione", JOptionPane.DEFAULT_OPTION, null, graficas, graficas[0]);
        if (respuesta != null) {
            String tipo = respuesta.toString();
            if (tipo.equals("Sin filtro")) {
                filtro = null;
            } else {
                filtro = tipo;
            }
        }

    }

    /**
     * Esta funcion usa los metodos estadisticos para calcular la frecuencia,
     * frecuencia relativa, frecuencia acumulada y frecuencia acumulada relativa
     * y los guarda en los arreglos de la clase correspondientes
     *
     * @param fechas Arreglo de fechas registradas en la base de datos
     */
    private void crearFrecuencias(ArrayList<String> fechas) {
        Collections.sort(fechas);
        int total = 0;
        for (int i = 0; i < fechas.size(); i++) {
            String tempo = mes(fechas.get(i).substring(5, 7)) + " " + año(fechas.get(i));
            if (filtro(año(fechas.get(i)))) {
                total++;

                if (!(clases.contains(tempo))) {
                    clases.add(tempo);
                }
            }
        }
        for (int j = 0; j < clases.size(); j++) {
            float frecuencia = 0;
            for (int i = 0; i < fechas.size(); i++) {
                if ((mes(fechas.get(i).substring(5, 7)) + " " + año(fechas.get(i))).equals(clases.get(j)) && filtro(año(fechas.get(i)))) {
                    frecuencia++;
                }
            }
            fi.add((int) frecuencia);
            firelativa.add(((frecuencia / total) * 100) + " %");

        }
        float sum = 0;
        for (int i = 0; i < fi.size(); i++) {
            sum = sum + fi.get(i);
            facum.add((int) sum);
            facumrelativa.add(((sum / total) * 100) + " %");
        }
    }

    /**
     * Esta funcion apartir de una fecha en formato yyyy/mm/dd sin distincion
     * del separador '/' retorna solo el año
     *
     * @param fecha
     * @return retorna el año
     */
    private String año(String fecha) {
        String a = fecha.substring(0, 4);
        return a;
    }

    /**
     * Esta funcion apartir de una fecha en formato yyyy/mm/dd sin distincion
     * del separador '/' retorna solo el mes de la siguiente forma:
     * <br><br>
     * 01 = Ene<br>
     * 02 = Feb<br>
     * 03 = Mar<br>
     * 04 = Abr<br>
     * 05 = May<br>
     * 06 = Jun<br>
     * 07 = Jul<br>
     * 08 = Ago<br>
     * 09 = Sep<br>
     * 10 = Oct<br>
     * 11 = Nov<br>
     * 12 = Dic<br>
     *
     * @param mes Mes de interaccion
     * @return nombre del mes
     */
    private String mes(String mes) {
        if (mes.equals("01")) {
            return "Ene";
        }
        if (mes.equals("02")) {
            return "Feb";
        }
        if (mes.equals("03")) {
            return "Mar";
        }
        if (mes.equals("04")) {
            return "Abr";
        }
        if (mes.equals("05")) {
            return "May";
        }
        if (mes.equals("06")) {
            return "Jun";
        }
        if (mes.equals("07")) {
            return "Jul";
        }
        if (mes.equals("08")) {
            return "Ago";
        }
        if (mes.equals("09")) {
            return "Sep";
        }
        if (mes.equals("10")) {
            return "Oct";
        }
        if (mes.equals("11")) {
            return "Nov";
        } else {
            return "Dic";
        }
    }
}
